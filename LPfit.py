import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt

def LPfit_fun(V, Isat, Te, Vfl):
    I=Isat*(1-np.exp((V-Vfl)/Te))
    return I


p0=[0.1, 1, 0.]

V=np.arange(-10, 2, 0.01)
plt.plot(V, LPfit_fun(V, *p0))
plt.show()

#popt, pcov = curve_fit(LPfit_fun, V_meas, data, p0=p0) #data is the data you measure, V_meas the voltage
